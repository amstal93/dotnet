# reductech/containers/dotnet/sdk

Docker image for testing .NET Core 3.1 applications on the GitLab Windows CI runner.

A fork of

- https://github.com/dotnet/dotnet-docker/blob/master/src/runtime/3.1/nanoserver-1809/amd64/Dockerfile
- https://github.com/dotnet/dotnet-docker/blob/master/src/aspnet/3.1/nanoserver-1809/amd64/Dockerfile
- https://github.com/dotnet/dotnet-docker/blob/master/src/sdk/3.1/nanoserver-1809/amd64/Dockerfile

but using a different base image for the runtime.

# Current versions

| Container | Version |
| :-------- | :-----: |
| Runtime | `3.1.9` |
| ASP.net | `3.1.9` |
| SDK | `3.1.403` |

# Pull the latest configs and build new images

```powershell
./update-and-build.ps1 -Build
```

There is also a `-Push` switch to update the registry.
